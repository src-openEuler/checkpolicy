Name:          checkpolicy
Version:       3.5
Release:       1
Summary:       SELinux policy compiler
License:       GPLv2
URL:           https://github.com/SELinuxProject/selinux
Source0:       https://github.com/SELinuxProject/selinux/releases/download/%{version}/checkpolicy-%{version}.tar.gz

BuildRequires: gcc byacc bison flex flex-static libsepol-static >= %{version} libselinux-devel >= %{version}

Conflicts: selinux-policy-base < 3.13.1-138

%description
checkpolicy is the SELinux policy compiler. It uses libsepol to
generate the binary policy.

(Security-enhanced Linux is a feature of the kernel and some
utilities that implement mandatory access control policies, such as
Type Enforcement, Role-based Access Control and Multi-Level
Security.)

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
make clean
make LIBDIR="%{_libdir}" CFLAGS="%{optflags}" LDFLAGS="$RPM_LD_FLAGS"
make -C test LIBDIR="%{_libdir}" CFLAGS="%{optflags}" LDFLAGS="$RPM_LD_FLAGS"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%make_install LIBDIR="%{_libdir}"
install test/dismod %{buildroot}%{_bindir}/sedismod
install test/dispol %{buildroot}%{_bindir}/sedispol

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license LICENSE
%{_bindir}/*

%files help
%defattr(-,root,root)
%{_mandir}/*/*

%changelog
* Mon Jul 17 2023 zhangguangzhi <zhangguangzhi3@huawei.com> - 3.5-1
- update version to 3.5

* Fri Mar 10 2023 zhangchenfeng <zhangchenfeng1@huawei.com> - 3.4-2
- backport upstrem bugfix

* Thu Feb 2 2023 zhangguangzhi <zhangguangzhi3@huawei.com> - 3.4-1
- update version to 3.4

* Tue Jan 18 2022 yixiangzhike <yixiangzhike007@163.com> - 3.3-1
- update to 3.3

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.1-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Jul 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1-1
- update to 3.1

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise description

* Fri Aug 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:strengthen spec

* Tue Aug 20 2019 guoxiaoqi<guoxiaoqi2@huawei.com> - 2.8-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:rename patches

* Tue Jul  9 2019 zhangyujing <zhangyujing1@huawei.com> - 2.8-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Destroy the class datum if it fails to initialize

* Thu Jul 12 2018 openEuler Buildteam <buildteam@openeuler.org> - 2.8-2
- Package init
